package ru.smart;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;


public class Application {

    private

    static int count  = 0;

    static void CombinationRepetitionUtil(int chosen[], int arr[], int index, int r, int start, int end,
                                          long starter, byte[] buffer) throws IOException {
            if (index == r) {

                String str = "";

                for (int i = 0; i < r; i++) {

                    str += " " + String.valueOf(arr[chosen[i]]);

                }

                str += "\n";

                buffer = str.getBytes();
                Arrays.toString(buffer);
                String string = new String(buffer);
                System.out.println(string);

                return;
            }

            for (int i = 0; i <= end; i++) {
                buffer = new byte[0];
                ByteArrayInputStream in = new ByteArrayInputStream(buffer);

                try(BufferedInputStream bis = new BufferedInputStream(in)){

                    chosen[index] = i;
                    CombinationRepetitionUtil(chosen, arr, index + 1, r, i, end,  starter, buffer );

                }
                catch(Exception e){
                    System.out.println(e.getMessage());
                }

            }

            long finish = System.currentTimeMillis();
            long elapsed = finish - starter;
         // System.out.println("Прошло времени, мс: " + elapsed);

            return;


    }

    static void CombinationRepetition(int arr[], int n, int r, long starter) throws IOException {

        int chosen[] = new int[r + 1];
        byte[] buffer = new byte[0];
        CombinationRepetitionUtil(chosen, arr, 0, r, 0, n - 1,  starter, buffer );

    }

    public static void main(String[] args) throws IOException {

        Scanner input = new Scanner(System.in);
        System.out.print("input size   : ");
        int size = input.nextInt();


        long starter = System.currentTimeMillis();
        int arr[];
        arr = new int[256];
        for(int k = 0; k<=255; k++ ){
            arr[k]= k;
        }
        int n = arr.length;
        int r = size* size;
        CombinationRepetition(arr, n, r, starter);
    }
}
